﻿using UnityEngine;

namespace Shooter2D.Enemy
{
    public class TrackerSpawner : Spawner
    {
        protected override void SpawnNewEnemy(EnemyConfigObject config)
        {
            TrackerConfigObject trackerConfig = config as TrackerConfigObject;

            var newEnemy = Instantiate(trackerConfig.EnemyPrefab, trackerConfig.SpawnPoint, Quaternion.identity);
            newEnemy.GetComponent<TrackerEnemyController>().TrackerConfigObject = trackerConfig;
        }
    }
}