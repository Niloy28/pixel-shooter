﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D.Enemy
{
    public abstract class Spawner : MonoBehaviour
    {
        [SerializeField] protected List<float> delays;
        [SerializeField] protected List<EnemyConfigObject> configs;
        [SerializeField] protected int noOfLoops;
        [SerializeField] protected bool isLooping;

        protected int currLoop;
        protected readonly int startIndex;

        protected void Awake()
        {
            PopulateEmptyDelayEntries();
            RemoveExtraDelayEntries();
        }

        protected void Start()
        {
            SetupProgressCounters();
            StartCoroutine(StartSpawn());
        }

        protected IEnumerator StartSpawn()
        {
            // wait before starting enemy spawns
            yield return new WaitForSecondsRealtime(delays[startIndex]);

            do
            {
                yield return StartCoroutine(SpawnAllConfigs());
                currLoop++;
            } while (isLooping || currLoop < noOfLoops);

            ProgressManager.Instance.IsThisSpawnerDone[this] = true;
        }

        private void SetupProgressCounters()
        {
            foreach (var config in configs)
            {
                ProgressManager.Instance.TargetEnemySpawnCount += config.NumberOfEnemies * noOfLoops;
            }
            ProgressManager.Instance.CurrentEnemySpawnCount = 0;
            ProgressManager.Instance.IsThisSpawnerDone[this] = false;
            ProgressManager.Instance.IsProgressCheckingAllowed = true;
        }

        protected IEnumerator SpawnAllConfigs()
        {
            for (int i = 0; i < configs.Count; ++i)
            {
                var config = configs[i];

                yield return StartCoroutine(SpawnEnemiesInConfig(config));
                yield return new WaitForSecondsRealtime(delays[i + 1]);
            }
        }

        protected IEnumerator SpawnEnemiesInConfig(EnemyConfigObject config)
        {
            var enemySpawnDelay = new WaitForSecondsRealtime(1 / config.EnemySpawnRate);

            for (int enemyCount = 0; enemyCount < config.NumberOfEnemies; enemyCount++)
            {
                SpawnNewEnemy(config);

                yield return enemySpawnDelay;
            }
        }

        protected abstract void SpawnNewEnemy(EnemyConfigObject config);

        protected void PopulateEmptyDelayEntries()
        {
            int lackingEntries = configs.Count - delays.Count + 1;

            for (int i = 0; i < lackingEntries; i++)
            {
                delays.Add(1);
            }
        }

        protected void RemoveExtraDelayEntries()
        {
            // keep one extra delay as the first delay will be before the spawning begins
            int removeFrom = Mathf.Max(0, configs.Count);
            int removeCount = Mathf.Max(0, delays.Count - configs.Count - 1);

            delays.RemoveRange(removeFrom, removeCount);
        }
    }
}