﻿using UnityEngine;

namespace Shooter2D.Enemy
{
    public class WaveSpawner : Spawner
    {
        protected override void SpawnNewEnemy(EnemyConfigObject config)
        {
            WaveConfigObject waveConfig = config as WaveConfigObject;

            var newEnemy = Instantiate(waveConfig.EnemyPrefab, waveConfig.WaveWaypoints[startIndex].position, Quaternion.identity);
            newEnemy.GetComponent<WaveEnemyController>().WaveConfigObject = waveConfig;
        }
    }
}