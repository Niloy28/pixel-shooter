﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class GameObjectShredder : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
    }
}
