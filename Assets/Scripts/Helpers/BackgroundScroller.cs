﻿using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    [SerializeField] private float scrollingSpeed = 0.4f;

    private Material mat;
    private Vector2 offset;

    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        offset = new Vector2(0f, scrollingSpeed);
    }

    private void Update()
    {
        mat.mainTextureOffset += offset * Time.deltaTime;
    }
}
