﻿using UnityEngine;

public class FitToCamera : MonoBehaviour
{
    private void Start()
    {
        Camera camera = Camera.main;
        var aspectRatio = camera.aspect;

        float verticalScale = camera.orthographicSize * 2;
        float horizontalScale = verticalScale * aspectRatio;

        transform.localScale = new Vector2(horizontalScale, verticalScale);
    }
}
