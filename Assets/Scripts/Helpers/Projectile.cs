﻿using UnityEngine;

namespace Shooter2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float projectileSpeed = 15f;

        private Rigidbody2D rb;

        private void Start()
        {
            SetupRigidBody();
            SetProjectileVelocity();
        }

        private void SetupRigidBody()
        {
            rb = GetComponent<Rigidbody2D>();
            rb.isKinematic = true;
        }

        private void SetProjectileVelocity()
        {
            rb.velocity = transform.up * projectileSpeed;
        }

        private void OnBecameInvisible()
        {
            Destroy(gameObject);
        }
    }
}