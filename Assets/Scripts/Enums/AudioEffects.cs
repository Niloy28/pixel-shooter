﻿namespace Shooter2D.Audio
{
    public enum AudioEffects
    {
        PlayerExplosion,
        EnemyExplosion,
        PlayerShoot,
        EnemyShoot,
        PowerUp,
        PowerDown,
        LevelCompleteFanfare,
        GameOver,
    }
}