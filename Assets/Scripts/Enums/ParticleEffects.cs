﻿namespace Shooter2D.Particles
{
    [System.Serializable]
    public enum ParticleEffects
    {
        PlayerExplosion,
        EnemyExplosion,
        PowerUp,
        PowerDown,
        GameOver,
    }
}