﻿using Shooter2D.Input;
using UnityEngine;

namespace Shooter2D.Player
{
    public class PlayerAnimationManager : MonoBehaviour
    {
        private Animator animator;

        public static PlayerAnimationManager Instance { get; set; }
        public bool ContinueAnimation { get; set; }

        private void Awake()
        {
            CacheReferences();
            ImplementSingleton();
            ContinueAnimation = true;
        }

        private void CacheReferences()
        {
            animator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
        }

        private void Update()
        {
            if (ContinueAnimation)
                animator.SetFloat("xSpeed", InputManager.Instance.MoveData.x);
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}