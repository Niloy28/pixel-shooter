﻿using Shooter2D.Audio;
using Shooter2D.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shooter2D
{
    public class LevelManager : MonoBehaviour
    {
        [Range(0f, 10f)]
        [SerializeField] private float loadDelay;

        private int sceneCount;

        public static LevelManager Instance { get; private set; }
        public static int LevelIndex { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            sceneCount = SceneManager.sceneCountInBuildSettings - 1;
        }

        public void LoadStartMenu()
        {
            LevelIndex = 0;
            SceneManager.LoadScene(0);
            MusicManager.Instance.SwitchMusicTrack(LevelIndex);
            ScoreManager.Instance.ResetScore();
        }

        public void LoadNextLevel()
        {
            LevelIndex++;
            if (LevelIndex > sceneCount)
                LevelIndex = 0;

            SceneManager.LoadScene(LevelIndex);
            MusicManager.Instance.SwitchMusicTrack(LevelIndex);
        }

        public void LoadLevel(int level)
        {
            LevelIndex = level;

            if (LevelIndex > sceneCount)
            {
                throw new System.Exception("wrong level int");
            }

            SceneManager.LoadScene(LevelIndex);
            MusicManager.Instance.SwitchMusicTrack(LevelIndex);
        }

        public void LoadLevelSelectMenu()
        {
            SceneManager.LoadScene("Level Select");
        }

        public void LoadGameOver()
        {
            // level select scene is the last scene in build settings.
            // so game over is sceneCount - 1
            SceneManager.LoadScene(sceneCount - 1);
            MusicManager.Instance.SwitchMusicTrack(sceneCount - 1);
            LevelIndex = sceneCount;
        }

        private IEnumerator GameLoadDelay(float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}