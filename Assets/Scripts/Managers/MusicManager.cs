using UnityEngine;

namespace Shooter2D.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicManager : MonoBehaviour
    {
        private AudioSource musicSource;
        private MusicDatabaseObject musicDatabase;

        public static MusicManager Instance { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            musicSource = GetComponent<AudioSource>();
            musicDatabase = Resources.Load<MusicDatabaseObject>("Database/Music Database");
            SwitchMusicTrack(0);
        }

        public void SwitchMusicTrack(int levelIndex)
        {
            musicSource.clip = musicDatabase.LevelMusic[levelIndex];
            musicSource.loop = musicDatabase.ClipToLoop[musicSource.clip];
            PlayNewTrack();
        }

        private void PlayNewTrack()
        {
            // stop old clip before playing new one
            musicSource.Stop();

            musicSource.Play();
        }

        public void StopMusicTrack()
        {
            musicSource.Pause();
        }
    }
}