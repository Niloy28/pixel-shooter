﻿using UnityEngine;

namespace Shooter2D.Particles
{
    public class ParticleEffectManager : MonoBehaviour
    {
        private ParticleDatabaseObject particleDatabase;

        public static ParticleEffectManager Instance { get; private set; }

        private void Awake()
        {
            LoadParticleDatabase();
            ImplementSingleton();
        }

        private void LoadParticleDatabase()
        {
            particleDatabase = Resources.Load("Database/Particle Database") as ParticleDatabaseObject;
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void CreateParticleEffect(ParticleEffects effectType, Transform transform)
        {
            var particle = particleDatabase.ParticleEffects[effectType];

            _ = Instantiate(particle, transform.position, Quaternion.identity);
        }
    }
}