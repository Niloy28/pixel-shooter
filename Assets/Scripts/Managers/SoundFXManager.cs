using UnityEngine;
using UnityEngine.Audio;

namespace Shooter2D.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundFXManager : MonoBehaviour
    {
        [SerializeField] private AudioMixerGroup masterGroup;
        [SerializeField] private AudioMixerGroup fxGroup;

        private AudioSource fxSource;
        private SoundFXDatabaseObject soundFXDatabase;

        public static SoundFXManager Instance { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            soundFXDatabase = Resources.Load<SoundFXDatabaseObject>("Database/SoundFX Database");
            fxSource = GetComponent<AudioSource>();
        }

        public void PlayAudioEffect(AudioEffects effectType)
        {
            if (effectType != AudioEffects.LevelCompleteFanfare)
            {
                fxSource.outputAudioMixerGroup = fxGroup;
                fxSource.PlayOneShot(soundFXDatabase.AudioEffects[effectType]);
            }
            else
            {
                fxSource.Stop();
                fxSource.outputAudioMixerGroup = masterGroup;
                fxSource.clip = soundFXDatabase.AudioEffects[effectType];
                fxSource.Play();
            }
        }
    }
}