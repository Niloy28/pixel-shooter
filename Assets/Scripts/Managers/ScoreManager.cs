﻿using UnityEngine;

namespace Shooter2D.UI
{
    public class ScoreManager : MonoBehaviour
    {
        public static ScoreManager Instance { get; private set; }
        public int Score { get; set; }

        private void Awake()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void ResetScore()
        {
            Score = 0;
        }
    }
}