﻿using UnityEngine;

namespace Shooter2D.Input
{
    public class InputManager : MonoBehaviour
    {
        private static PlayerInputs playerInputs;

        public static InputManager Instance { get; private set; }
        public Vector2 MoveData { get; private set; }
        public bool IsShooting { get; private set; }

        private void Awake()
        {
            ImplementSingleton();
            SetupPlayerInputs();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void SetupPlayerInputs()
        {
            playerInputs = new PlayerInputs();

            playerInputs.Gameplay.Movement.performed += ctx => MoveData = ctx.ReadValue<Vector2>();
            playerInputs.Gameplay.Movement.canceled += _ => MoveData = Vector2.zero;

            playerInputs.Gameplay.Shoot.performed += _ => IsShooting = true;
            playerInputs.Gameplay.Shoot.canceled += _ => IsShooting = false;
        }

        private void OnEnable()
        {
            playerInputs.Enable();
        }

        private void OnDisable()
        {
            playerInputs.Disable();
        }
    }
}