using Shooter2D.Audio;
using Shooter2D.Enemy;
using Shooter2D.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Shooter2D
{
    public class ProgressManager : MonoBehaviour
    {
        private PlayerController player;
        private bool isLevelDone;
        private bool isFanfarInitiated;
        private bool isFanfareDone;
        private bool isPlayerOutOfView;

        public static ProgressManager Instance { get; private set; }
        public int TargetEnemySpawnCount { get; set; }
        public int CurrentEnemySpawnCount { get; set; }
        public bool IsProgressCheckingAllowed { get; set; }
        public Dictionary<Spawner, bool> IsThisSpawnerDone { get; set; } = new Dictionary<Spawner, bool>();

        private void Awake()
        {
            ImplementSingleton();
        }

        private void Update()
        {
            if (LevelManager.LevelIndex != SceneManager.GetSceneByName("Game Over").buildIndex)
            {
                if (IsProgressCheckingAllowed && !isLevelDone)
                {
                    CheckIfAllSpawnersDone();
                }

                if (isLevelDone && !isPlayerOutOfView)
                {
                    LevelEndSequence();
                }

                if (isPlayerOutOfView && isFanfareDone)
                {
                    ResetProgressCounters();
                    LevelManager.Instance.LoadNextLevel();
                    ResetPlayer();
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void ResetPlayer()
        {
            player.enabled = true;
            player.gameObject.transform.position = new Vector2(0f, -4.35f);
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void CheckIfAllSpawnersDone()
        {
            var isSpawnerDoneBoolList = IsThisSpawnerDone.Values.ToList();
            if (!isSpawnerDoneBoolList.Contains(false) && CurrentEnemySpawnCount == TargetEnemySpawnCount)
            {
                isLevelDone = true;
            }
        }

        private void LevelEndSequence()
        {
            if (!isFanfarInitiated)
            {
                MusicManager.Instance.StopMusicTrack();
                SoundFXManager.Instance.PlayAudioEffect(AudioEffects.LevelCompleteFanfare);

                StartCoroutine(FanfareDelay());
                isFanfarInitiated = true;
            }

            if (player == null)
            {
                player = FindObjectOfType<PlayerController>();
            }
            player.enabled = false;

            if (Vector2.Distance(player.transform.position, new Vector2(player.transform.position.x, 8)) > 0.001f)
            {
                player.transform.position = Vector3.MoveTowards(player.transform.position, new Vector2(player.transform.position.x, 8), Time.deltaTime * 5);
            }
            else
            {
                isPlayerOutOfView = true;
            }
        }

        private IEnumerator FanfareDelay()
        {
            yield return new WaitForSecondsRealtime(4.41f);
            isFanfareDone = true;
        }

        private void ResetProgressCounters()
        {
            TargetEnemySpawnCount = 0;
            CurrentEnemySpawnCount = 0;
            IsThisSpawnerDone.Clear();
            isLevelDone = false;
            isFanfarInitiated = false;
            isFanfareDone = false;
            isPlayerOutOfView = false;
        }
    }
}