﻿using UnityEngine;

namespace Shooter2D
{
    public class Score : MonoBehaviour
    {
        [SerializeField] private int perScore;

        public int PerScore => perScore;
    }
}