﻿using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D.Enemy
{
    public class WaveEnemyController : MonoBehaviour
    {
        private List<Transform> waypoints;
        private int waypointIndex;
        private float movementSpeed;

        public WaveConfigObject WaveConfigObject { get; set; }

        private void Start()
        {
            SetupEnemy();
        }

        private void Update()
        {
            if (EnemyReachedTarget())
            {
                ProgressManager.Instance.CurrentEnemySpawnCount++;

                Destroy(gameObject);
            }
            else
            {
                MoveEnemyAlongPath();
            }
        }

        private void MoveEnemyAlongPath()
        {
            var targetPos = waypoints[waypointIndex].position;

            transform.position = Vector2.MoveTowards(transform.position, targetPos, movementSpeed * Time.deltaTime);

            if (transform.position == targetPos) { waypointIndex++; }
        }

        private void SetupEnemy()
        {
            movementSpeed = WaveConfigObject.EnemyMovementSpeed;
            waypoints = WaveConfigObject.WaveWaypoints;
            transform.position = waypoints[waypointIndex++].position;
        }

        private bool EnemyReachedTarget()
        {
            return waypointIndex == waypoints.Count;
        }
    }
}