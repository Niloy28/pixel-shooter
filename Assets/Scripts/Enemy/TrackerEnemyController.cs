﻿using UnityEngine;

namespace Shooter2D.Enemy
{
    public class TrackerEnemyController : MonoBehaviour
    {
        private Transform playerTransform;
        private float movementSpeed;

        public TrackerConfigObject TrackerConfigObject { get; set; }

        private void Start()
        {
            SetupEnemy();
            FindPlayerTarget();
        }

        private void Update()
        {
            if (playerTransform != null)
            {
                TrackPlayerTarget();
            }
        }

        private void SetupEnemy()
        {
            movementSpeed = TrackerConfigObject.EnemyMovementSpeed;
        }

        private void FindPlayerTarget()
        {
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        }

        private void TrackPlayerTarget()
        {
            transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, movementSpeed * Time.deltaTime);
            Vector3 newDirection = (playerTransform.position - transform.position).normalized;

            transform.rotation = Quaternion.LookRotation(Vector3.forward, newDirection);
        }
    }
}