﻿using Shooter2D.Audio;

namespace Shooter2D.Enemy
{
    public class EnemyShooting : Shooting
    {
        private bool isInView;

        private void Update()
        {
            if (isShootingAllowed && isInView)
            {
                SoundFXManager.Instance.PlayAudioEffect(AudioEffects.EnemyShoot);
                _ = StartCoroutine(Shoot());
            }
        }

        private void OnBecameVisible()
        {
            isInView = true;
        }

        private void OnBecameInvisible()
        {
            isInView = false;
        }
    }
}