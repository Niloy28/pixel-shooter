﻿using Shooter2D.Audio;
using Shooter2D.Particles;
using Shooter2D.UI;

namespace Shooter2D.Enemy
{
    public class EnemyHealth : EntityHealth
    {
        protected override void HandleHit(int damage)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                Explode();
            }
        }

        protected override void Explode()
        {
            ParticleEffectManager.Instance.CreateParticleEffect(ParticleEffects.EnemyExplosion, transform);
            SoundFXManager.Instance.PlayAudioEffect(AudioEffects.EnemyExplosion);
            ScoreManager.Instance.Score += GetComponent<Score>().PerScore;

            var powerUp = PowerUpSpawner.DropRandomPowerUp();
            if (powerUp != null)
            {
                powerUp.transform.position = transform.position;
            }

            ProgressManager.Instance.CurrentEnemySpawnCount++;

            Destroy(gameObject);
        }
    }
}