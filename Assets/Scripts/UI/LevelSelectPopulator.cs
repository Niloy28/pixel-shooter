using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Shooter2D
{
    public class LevelSelectPopulator : MonoBehaviour
    {
        [SerializeField] private GameObject buttonParent;
        [SerializeField] private Button buttonPrefab;

        private void Start()
        {
            // last 2 scenes are game over and level select
            // level index starts from 0 so minus 3
            int levelCount = SceneManager.sceneCountInBuildSettings - 3;

            bool isFirstButton = true;
            // start count from 1 since scene 0 is start menu
            for (int i = 1; i <= levelCount; i++)
            {
                int levelIndex = i;
                Button button = Instantiate(buttonPrefab, buttonParent.transform);
                button.onClick.AddListener(() => LevelManager.Instance.LoadLevel(levelIndex));
                button.GetComponentInChildren<TMP_Text>().text = levelIndex.ToString();

                if (isFirstButton)
                {
                    isFirstButton = false;
                    EventSystem.current.SetSelectedGameObject(button.gameObject);
                }
            }
        }
    }
}