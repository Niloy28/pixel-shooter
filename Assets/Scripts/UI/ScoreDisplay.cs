﻿using TMPro;
using UnityEngine;

namespace Shooter2D.UI
{
    public class ScoreDisplay : MonoBehaviour
    {
        private TextMeshProUGUI scoreText;

        private void Awake()
        {
            scoreText = GetComponent<TextMeshProUGUI>();
        }

        private void Update()
        {
            string text = string.Format("Score: {0}", ScoreManager.Instance.Score);

            scoreText.text = text;
        }
    }
}