﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Shooter2D.UI
{
    public class HealthDisplay : MonoBehaviour
    {
        [Header("Health Bar")]
        [SerializeField] private Slider slider;
        [SerializeField] private Gradient gradient;

        [Header("Extra Lives")]
        [SerializeField] Transform extraLifeHolder;
        [SerializeField] private GameObject extraLifePrefab;

        private Image fillImage;
        private Stack<GameObject> createdExtraLives;
        private int extraLives;

        public int ExtraLives
        {
            get
            {
                return extraLives;
            }
            set
            {
                int oldExtraLives = extraLives;

                extraLives = value;

                if (oldExtraLives < ExtraLives)
                {
                    CreateExtraLifeUI();
                }
                else if (oldExtraLives > ExtraLives)
                {
                    RemoveExtraLifeUI();
                }
            }
        }
        public int Health
        {
            set
            {
                slider.value = value;
                AdjustFillColor();
            }
        }
        public int MaxHealth
        {
            set
            {
                slider.maxValue = value;
                slider.value = value;
                AdjustFillColor();
            }
        }

        private void Awake()
        {
            CacheReferences();
            InitializeStack();
        }

        private void Start()
        {
            SetupHealthDisplay();
        }

        private void SetupHealthDisplay()
        {
            slider.value = slider.maxValue;
            for (int i = 1; i < ExtraLives; i++)
            {
                CreateExtraLifeUI();
            }
        }

        private void CacheReferences()
        {
            fillImage = slider.fillRect.gameObject.GetComponent<Image>();
        }

        private void InitializeStack()
        {
            createdExtraLives = new Stack<GameObject>();
        }

        private void AdjustFillColor()
        {
            fillImage.color = gradient.Evaluate(slider.normalizedValue);
        }

        private void CreateExtraLifeUI()
        {
            GameObject extraLifeObject = Instantiate(extraLifePrefab, extraLifeHolder);

            createdExtraLives.Push(extraLifeObject);
        }

        private void RemoveExtraLifeUI()
        {
            if (createdExtraLives.Count != 0)
                Destroy(createdExtraLives.Pop());
        }
    }
}