using UnityEngine;

namespace Shooter2D.UI
{
    public class HealthDisplayPersistence : MonoBehaviour
    {
        private void Awake()
        {
            PersistHealthDisplayAcrossLevels();
        }

        private void PersistHealthDisplayAcrossLevels()
        {
            if (FindObjectsOfType<HealthDisplayPersistence>().Length > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}