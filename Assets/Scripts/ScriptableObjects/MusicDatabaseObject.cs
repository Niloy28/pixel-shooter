using UnityEngine;

namespace Shooter2D.Audio
{
    [CreateAssetMenu(fileName = "New Music Database", menuName = "Database/Music Database")]
    public class MusicDatabaseObject : ScriptableObject
    {
        [SerializeField] private SerializableDictionary<int, AudioClip> levelMusic;
        [SerializeField] private SerializableDictionary<AudioClip, bool> clipToLoop;

        public SerializableDictionary<int, AudioClip> LevelMusic => levelMusic;
        public SerializableDictionary<AudioClip, bool> ClipToLoop => clipToLoop;
    }
}