using UnityEngine;

namespace Shooter2D.Audio
{
    [CreateAssetMenu(fileName = "New SoundFX Database", menuName = "Database/SoundFX Database")]
    public class SoundFXDatabaseObject : ScriptableObject
    {
        [SerializeField] private SerializableDictionary<AudioEffects, AudioClip> audioEffects;

        public SerializableDictionary<AudioEffects, AudioClip> AudioEffects => audioEffects;
    }
}