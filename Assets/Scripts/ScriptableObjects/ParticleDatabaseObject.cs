﻿using UnityEngine;

namespace Shooter2D.Particles
{
    [CreateAssetMenu(fileName = "New Particle Database", menuName = "Database/Particle Database")]
    public class ParticleDatabaseObject : ScriptableObject
    {
        [SerializeField] private SerializableDictionary<ParticleEffects, GameObject> particleEffects = new SerializableDictionary<ParticleEffects, GameObject>();

        public SerializableDictionary<ParticleEffects, GameObject> ParticleEffects => particleEffects;
    }
}