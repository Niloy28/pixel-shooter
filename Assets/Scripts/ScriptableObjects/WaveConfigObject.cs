﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D.Enemy
{
    [CreateAssetMenu(menuName = "Enemy Configs/New Wave Config")]
    public class WaveConfigObject : EnemyConfigObject
    {
        [SerializeField] private GameObject pathPrefab;
        [SerializeField] private Vector3 pathPosition;
        [SerializeField] private Quaternion pathRotation;
        [SerializeField] private bool isMirrored;

        public List<Transform> WaveWaypoints
        {
            get
            {
                List<Transform> waveWaypoints = new List<Transform>();

                pathPrefab.transform.rotation = pathRotation;
                foreach (Transform child in pathPrefab.transform)
                {
                    //child.position += pathPosition;
                    waveWaypoints.Add(child);
                }
                if (isMirrored) { waveWaypoints.Reverse(); }

                return waveWaypoints;
            }
        }
    }
}