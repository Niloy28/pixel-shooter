﻿using UnityEngine;

namespace Shooter2D.Enemy
{
    public abstract class EnemyConfigObject : ScriptableObject
    {
        [SerializeField] protected GameObject enemyPrefab;
        [SerializeField] protected int numberOfEnemies = 10;

        [Range(0f, 10f)]
        [SerializeField] protected float enemySpawnRate = 0.5f;

        [Range(0f, 10f)]
        [SerializeField] protected float enemyMovementSpeed = 2f;

        public GameObject EnemyPrefab => enemyPrefab;
        public int NumberOfEnemies => numberOfEnemies;
        public float EnemySpawnRate => enemySpawnRate;
        public float EnemyMovementSpeed => enemyMovementSpeed;
    }
}