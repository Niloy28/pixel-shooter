﻿using UnityEngine;

namespace Shooter2D.Enemy
{
    [CreateAssetMenu(menuName = "Enemy Configs/New Tracker Config")]
    public class TrackerConfigObject : EnemyConfigObject
    {
        [SerializeField] private Vector2 spawnPoint;

        public Vector2 SpawnPoint => spawnPoint;
    }
}