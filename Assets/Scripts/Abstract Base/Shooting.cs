﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shooter2D
{
    public abstract class Shooting : MonoBehaviour
    {
        [SerializeField] protected GameObject bullet;
        [SerializeField] protected List<Vector2> shootPoints;

        [Range(0f, 10f)]
        [SerializeField] protected float shootingCooldown;

        protected WaitForSecondsRealtime shootingDelay;
        protected bool isShootingAllowed = true;

        protected void Awake()
        {
            shootingDelay = new WaitForSecondsRealtime(shootingCooldown);
        }

        protected IEnumerator Shoot()
        {
            foreach (Vector3 point in shootPoints)
            {
                _ = Instantiate(bullet, point + transform.position, transform.rotation);
            }

            isShootingAllowed = false;
            yield return shootingDelay;
            isShootingAllowed = true;
        }
    }
}