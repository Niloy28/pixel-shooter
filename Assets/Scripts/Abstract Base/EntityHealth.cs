﻿using UnityEngine;
using System.Collections.Generic;

namespace Shooter2D
{
    public abstract class EntityHealth : MonoBehaviour
    {
        [SerializeField] protected int maxHealth;
        [SerializeField] protected List<string> compareTags;

        protected int currentHealth;

        protected virtual void Awake()
        {
            currentHealth = maxHealth;
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (compareTags.Contains(collision.tag))
            {
                int damage = collision.gameObject.GetComponent<DamageDealer>().DamageDealt;
                if (collision.CompareTag("Enemy"))
                {
                    collision.GetComponent<EntityHealth>().HandleHit(10);
                }
                else
                {
                    Destroy(collision.gameObject);
                }

                HandleHit(damage);
            }
        }

        protected abstract void HandleHit(int damage);

        protected abstract void Explode();
    }
}