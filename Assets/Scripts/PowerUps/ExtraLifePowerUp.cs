using Shooter2D.Player;

namespace Shooter2D
{
    public class ExtraLifePowerUp : PowerUp
    {
        public override PowerUpType PowerUpType => PowerUpType.ExtraLife;

        protected override void ProcessPowerUp()
        {
            FindObjectOfType<PlayerHealth>().ExtraLives++;

            Destroy(gameObject);
        }
    }
}