using UnityEngine;

namespace Shooter2D
{
    public class PowerUpFactory : MonoBehaviour
    {
        [SerializeField] private GameObject healthPowerUpPrefab;
        [SerializeField] private GameObject extraLifePrefab;

        public static PowerUpFactory Instance { get; private set; }

        private void Start()
        {
            ImplementSingleton();
        }

        private void ImplementSingleton()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public GameObject GeneratePowerUp(PowerUpType powerUpType)
        {
            switch (powerUpType)
            {
                case PowerUpType.Health:
                    return Instantiate(healthPowerUpPrefab);

                case PowerUpType.ExtraLife:
                    return Instantiate(extraLifePrefab);
            }
            return null;
        }
    }
}