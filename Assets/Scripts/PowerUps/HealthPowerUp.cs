using Shooter2D.Player;

namespace Shooter2D
{
    public class HealthPowerUp : PowerUp
    {
        public override PowerUpType PowerUpType => PowerUpType.Health;

        protected override void ProcessPowerUp()
        {
            FindObjectOfType<PlayerHealth>().CurrentHealth += 15;

            Destroy(gameObject);
        }
    }
}