using UnityEngine;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Shooter2D
{
    public static class PowerUpSpawner
    {
        private static float healthPowerUpProbability;
        private static float extraLifeProbability;
        private static int powerUpDropCooldown;
        private static int powerUpSpawnCountDown;

        static PowerUpSpawner()
        {
            string path = Path.Combine(Application.streamingAssetsPath, "powerup.json");

            using (StreamReader r = new StreamReader(path))
            {
                string jsonData = r.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Dictionary<string, float>>(jsonData);
                healthPowerUpProbability = data["healthPowerUpProbability"];
                extraLifeProbability = data["extraLifeProbability"];
                powerUpDropCooldown = (int)data["powerUpDropCooldown"];
            }
        }

        public static GameObject DropRandomPowerUp()
        {
            if (powerUpSpawnCountDown > 0)
            {
                powerUpSpawnCountDown--;
                return null;
            }

            int generatedRandom = Random.Range(0, 101);

            if (generatedRandom <= extraLifeProbability * 100)
            {
                powerUpSpawnCountDown = powerUpDropCooldown;
                return PowerUpFactory.Instance.GeneratePowerUp(PowerUpType.ExtraLife);
            }
            else if (generatedRandom <= healthPowerUpProbability * 100)
            {
                powerUpSpawnCountDown = powerUpDropCooldown;
                return PowerUpFactory.Instance.GeneratePowerUp(PowerUpType.Health);
            }
            else
            {
                return null;
            }
        }
    }
}