using UnityEngine;

namespace Shooter2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(CircleCollider2D))]
    public abstract class PowerUp : MonoBehaviour
    {
        public abstract PowerUpType PowerUpType { get; }

        protected void Start()
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -5f);
        }

        protected void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                ProcessPowerUp();
            }
        }

        protected abstract void ProcessPowerUp();
    }
}