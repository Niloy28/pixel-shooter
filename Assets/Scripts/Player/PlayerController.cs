﻿using UnityEngine;
using Shooter2D.Input;

namespace Shooter2D.Player
{
    public class PlayerController : MonoBehaviour
    {
        [Range(0f, 10f)][SerializeField] private float movementSpeed = 5f;

        private SpriteRenderer spriteRenderer;
        private Vector2 minBoundary;
        private Vector2 maxBoundary;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            SetupBoundaries();
        }

        private void Update()
        {
            MovePlayer();
        }

        private void SetupBoundaries()
        {
            Camera camera = Camera.main;

            minBoundary = camera.ViewportToWorldPoint(new Vector2(0, 0));
            maxBoundary = camera.ViewportToWorldPoint(new Vector2(1, 1));
        }

        private void MovePlayer()
        {
            Vector2 movement = movementSpeed * InputManager.Instance.MoveData * Time.deltaTime;

            transform.Translate(movement);

            ClampPlayerPosition();
        }

        private void ClampPlayerPosition()
        {
            var spriteCenterOffset = spriteRenderer.size / 2;

            float adjustedX = Mathf.Clamp(transform.position.x, minBoundary.x + spriteCenterOffset.x, maxBoundary.x - spriteCenterOffset.x);
            float adjustedY = Mathf.Clamp(transform.position.y, minBoundary.y + spriteCenterOffset.y, maxBoundary.y - spriteCenterOffset.y);

            transform.position = new Vector2(adjustedX, adjustedY);
        }
    }
}