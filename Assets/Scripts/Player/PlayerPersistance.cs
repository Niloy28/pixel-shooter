using UnityEngine;

namespace Shooter2D.Player
{
    public class PlayerPersistance : MonoBehaviour
    {
        private void Awake()
        {
            PersistPlayerAcrossLevels();
        }

        private void PersistPlayerAcrossLevels()
        {
            if (FindObjectsOfType<PlayerPersistance>().Length > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}