﻿using Shooter2D.Input;
using Shooter2D.Audio;

namespace Shooter2D.Player
{
    public class PlayerShooting : Shooting
    {
        private void Update()
        {
            if (InputManager.Instance.IsShooting && isShootingAllowed)
            {
                SoundFXManager.Instance.PlayAudioEffect(AudioEffects.PlayerShoot);
                _ = StartCoroutine(Shoot());
            }
        }
    }
}