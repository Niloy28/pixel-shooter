﻿using Shooter2D.Audio;
using Shooter2D.Particles;
using Shooter2D.UI;
using System.Collections;
using UnityEngine;

namespace Shooter2D.Player
{
    public class PlayerHealth : EntityHealth
    {
        [SerializeField] private HealthDisplay healthDisplay;
        [SerializeField] private int extraLives;

        [Header("Player Specific")]
        [SerializeField] private float playerImmunityDuration = 1.5f;
        [SerializeField] private float blinkFreq = 5f;

        private SpriteRenderer spriteRenderer;
        private bool isImmune;
        private WaitForSeconds perBlinkDelay;

        public int CurrentHealth {
            get => currentHealth;

            set {
                currentHealth = value;
                currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
                healthDisplay.Health = currentHealth;
            }
        }

        public int ExtraLives {
            get => extraLives;
            set {
                extraLives = value;
                healthDisplay.ExtraLives = extraLives;
            }
        
        }

        protected override void Awake()
        {
            base.Awake();
            healthDisplay.MaxHealth = maxHealth;
            healthDisplay.ExtraLives = ExtraLives;
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected override void HandleHit(int damage)
        {
            if (isImmune) return;

            CurrentHealth -= damage;
            StartCoroutine(PlayerImmuneTimer());

            if (CurrentHealth <= 0)
            {
                ExtraLives--;

                if (ExtraLives < 0)
                {
                    Explode();
                }
                else
                {
                    CurrentHealth = maxHealth;
                }
            }
        }

        protected override void Explode()
        {
            PlayerAnimationManager.Instance.ContinueAnimation = false;
            ParticleEffectManager.Instance.CreateParticleEffect(ParticleEffects.PlayerExplosion, transform);
            SoundFXManager.Instance.PlayAudioEffect(AudioEffects.PlayerExplosion);
            Destroy(healthDisplay.gameObject);
            Destroy(gameObject);
            LevelManager.Instance.LoadGameOver();
        }

        private IEnumerator PlayerImmuneTimer()
        {
            StartCoroutine(PlayerBlink());

            isImmune = true;
            yield return new WaitForSeconds(playerImmunityDuration);
            isImmune = false;
        }

        private IEnumerator PlayerBlink()
        {
            int blinkNumber = (int)(playerImmunityDuration * blinkFreq);
            float perBlinkDuration = playerImmunityDuration / blinkNumber;

            if (perBlinkDelay == null)
            {
                perBlinkDelay = new WaitForSeconds(perBlinkDuration);
            }

            for (int i = 0; i < blinkNumber; i++)
            {
                spriteRenderer.enabled = false;
                yield return perBlinkDelay;
                spriteRenderer.enabled = true;
                yield return perBlinkDelay;
            }
        }
    }
}